const CM_MODULE_REGISTER_BASE: usize = 0x44E10000;

pub struct IOPin {
    register: PinRegister,
}
#[allow(dead_code)]
#[allow(non_camel_case_types)]
#[derive(Clone, Copy, Debug)]
pub enum PinRegister {
    conf_gpmc_ad0 = 0x800,
    conf_gpmc_ad1 = 0x804,
    conf_gpmc_ad2 = 0x808,
    conf_gpmc_ad3 = 0x80C,
    conf_gpmc_ad4 = 0x810,
    conf_gpmc_ad5 = 0x814,
    conf_gpmc_ad6 = 0x818,
    conf_gpmc_ad7 = 0x81C,
    conf_gpmc_ad8 = 0x820,
    conf_gpmc_ad9 = 0x824,
    conf_gpmc_ad10 = 0x828,
    conf_gpmc_ad11 = 0x82C,
    conf_gpmc_ad12 = 0x830,
    conf_gpmc_ad13 = 0x834,
    conf_gpmc_ad14 = 0x838,
    conf_gpmc_ad15 = 0x83C,
    conf_gpmc_a0 = 0x840,
    conf_gpmc_a1 = 0x844,
    conf_gpmc_a2 = 0x848,
    conf_gpmc_a3 = 0x84C,
    conf_gpmc_a4 = 0x850,
    conf_gpmc_a5 = 0x854,
    conf_gpmc_a6 = 0x858,
    conf_gpmc_a7 = 0x85C,
    conf_gpmc_a8 = 0x860,
    conf_gpmc_a9 = 0x864,
    conf_gpmc_a10 = 0x868,
    conf_gpmc_a11 = 0x86C,
    conf_gpmc_wait0 = 0x870,
    conf_gpmc_wpn = 0x874,
    conf_gpmc_ben1 = 0x878,
    conf_gpmc_csn0 = 0x87C,
    conf_gpmc_csn1 = 0x880,
    conf_gpmc_csn2 = 0x884,
    conf_gpmc_csn3 = 0x888,
    conf_gpmc_clk = 0x88C,
    conf_gpmc_advn_ale = 0x890,
    conf_gpmc_wen = 0x898,
    conf_gpmc_ben0_cle = 0x89C,
    conf_lcd_data0 = 0x8A0,
    conf_lcd_data1 = 0x8A4,
    conf_lcd_data2 = 0x8A8,
    conf_lcd_data3 = 0x8AC,
    conf_lcd_data4 = 0x8B0,
    conf_lcd_data5 = 0x8B4,
    conf_lcd_data6 = 0x8B8,
    conf_lcd_data7 = 0x8BC,
    conf_lcd_data8 = 0x8C0,
    conf_lcd_data9 = 0x8C4,
    conf_lcd_data10 = 0x8C8,
    conf_lcd_data11 = 0x8CC,
    conf_lcd_data12 = 0x8D0,
    conf_lcd_data13 = 0x8D4,
    conf_lcd_data14 = 0x8D8,
    conf_lcd_data15 = 0x8DC,
    conf_lcd_vsync = 0x8E0,
    conf_lcd_hsync = 0x8E4,
    conf_lcd_pclk = 0x8E8,
    conf_lcd_ac_bias_en = 0x8EC,
    conf_mmc0_dat3 = 0x8F0,
    conf_mmc0_dat2 = 0x8F4,
    conf_mmc0_dat1 = 0x8F8,
    conf_mmc0_dat0 = 0x8FC,
    conf_mmc0_clk = 0x900,
    conf_mmc0_cmd = 0x904,
    conf_mii1_col = 0x908,
    conf_mii1_crs = 0x90C,
    conf_mii1_rx_er = 0x910,
    conf_mii1_tx_en = 0x914,
    conf_mii1_rx_dv = 0x918,
    conf_mii1_txd3 = 0x91C,
    conf_mii1_txd2 = 0x920,
    conf_mii1_txd1 = 0x924,
    conf_mii1_txd0 = 0x928,
    conf_mii1_tx_clk = 0x92C,
    conf_mii1_rx_clk = 0x930,
    conf_mii1_rxd3 = 0x934,
    conf_mii1_rxd2 = 0x938,
    conf_mii1_rxd1 = 0x93C,
    conf_mii1_rxd0 = 0x940,
    conf_rmii1_ref_clk = 0x944,
    conf_mdio = 0x948,
    conf_mdc = 0x94C,
    conf_spi0_d0 = 0x954,
    conf_spi0_d1 = 0x958,
    conf_spi0_cs0 = 0x95C,
    conf_spi0_cs1 = 0x960,
    conf_ecap0_in_pwm0_out = 0x964,
    conf_uart0_ctsn = 0x968,
    conf_uart0_rtsn = 0x96C,
    conf_uart0_rxd = 0x970,
    conf_uart0_txd = 0x974,
    conf_uart1_ctsn = 0x978,
    conf_uart1_rtsn = 0x97C,
    conf_uart1_rxd = 0x980,
    conf_uart1_txd = 0x984,
    conf_i2c0_sda = 0x988,
    conf_i2c0_scl = 0x98C,
    conf_mcasp0_aclkx = 0x990,
    conf_mcasp0_fsx = 0x994,
    conf_mcasp0_axr0 = 0x998,
    conf_mcasp0_ahclkr = 0x99C,
    conf_mcasp0_aclkr = 0x9A0,
    conf_mcasp0_fsr = 0x9A4,
    conf_mcasp0_axr1 = 0x9A8,
    conf_mcasp0_ahclkx = 0x9AC,
    conf_xdma_event_intr0 = 0x9B0,
    conf_xdma_event_intr1 = 0x9B4,
    conf_warmrstn = 0x9B8,
    conf_nnmi = 0x9C0,
    conf_tms = 0x9D0,
    conf_tdi = 0x9D4,
    conf_tdo = 0x9D8,
    conf_tck = 0x9DC,
    conf_trstn = 0x9E0,
    conf_emu0 = 0x9E4,
    conf_emu1 = 0x9E8,
    conf_rtc_pwronrstn = 0x9F8,
    conf_pmic_power_en = 0x9FC,
    conf_ext_wakeup = 0xA00,
    conf_usb0_drvvbus = 0xA1C,
    conf_usb1_drvvbus = 0xA34,
}

pub enum PinMode {
    Mode0,
    Mode1,
    Mode2,
    Mode3,
    Mode4,
    Mode5,
    Mode6,
    Mode7,
}
pub enum PullType {
    Pulldown,
    Pullup,
}
pub enum SlewMode {
    Slow,
    Fast,
}

impl IOPin {
    pub fn new(register: PinRegister) -> Self {
        IOPin { register }
    }

    pub unsafe fn set(
        &self,
        mode: Option<PinMode>,
        input_enable: Option<bool>,
        pull: Option<(PullType, bool)>,
        slew: Option<SlewMode>,
    ) {
        let mut reg = super::mem_read!(CM_MODULE_REGISTER_BASE + (self.register as usize));
        if let Some(mode) = mode {
            //set mode bits to zero
            reg &= !(0b111);
            reg |= match mode {
                PinMode::Mode0 => 0,
                PinMode::Mode1 => 1,
                PinMode::Mode2 => 2,
                PinMode::Mode3 => 3,
                PinMode::Mode4 => 4,
                PinMode::Mode5 => 5,
                PinMode::Mode6 => 6,
                PinMode::Mode7 => 7,
            };
        }
        if let Some(input_enable) = input_enable {
            reg &= !(1 << 5);
            reg |= (input_enable as usize) << 5;
        }
        if let Some((pull_type, pull_enable)) = pull {
            reg &= !(0b11 << 3);
            let mut temp = match pull_type {
                PullType::Pulldown => 0,
                PullType::Pullup => 1,
            } << 4;
            temp |= (!(pull_enable as usize)) << 3;
            reg |= temp;
        }

        if let Some(slew) = slew {
            reg &= !(1 << 6);
            reg |= match slew {
                SlewMode::Slow => 0,
                SlewMode::Fast => 1,
            } << 6;
        }

        super::mem_write!(CM_MODULE_REGISTER_BASE + (self.register as usize), reg);
    }
}
