use super::*;

const UART0_BASE: usize = 0x44E09000;
const UART_SYSC: usize = 0x54;
const UART_SYSS: usize = 0x58;
const UART_EFR: usize = 0x8;
const UART_LCR: usize = 0xC;

const CKM_PER: usize = 0x44E00000;
const CKM_WKUP: usize = 0x44E00400;
const CKM_PER_GPIO1_CLKCTRL: usize = 0x0ac;
const CKM_PER_L4HS_CLKSTCTRL: usize = 0x11C;
const CKM_WKUP_CLKSTCTRL: usize = 0x0;
const CKM_WKUP_UART0_CLKCTRL: usize = 0xb4;

const CM_MODULE_REGISTER_BASE: usize = 0x44E10000;

pub struct Uart {
    pub device: UartDevice,
}

use clock::*;
use control::*;

pub enum UartDevice {
    UART0,
    UART1,
    UART2,
    UART3,
    UART4,
    UART5,
}

impl UartDevice{
    pub fn get_base(&self) -> usize{
        match self{
            UartDevice::UART0 => 0x44E09000,
            UartDevice::UART1 => todo!(),
            UartDevice::UART2 => todo!(),
            UartDevice::UART3 => todo!(),
            UartDevice::UART4 => 0x481A8000,
            UartDevice::UART5 => todo!(),
        }
    }
}

impl Uart {
    pub unsafe fn init(&self) {
        match self.device {
            UartDevice::UART0 => uart0_init(),
            UartDevice::UART1 => todo!(),
            UartDevice::UART2 => todo!(),
            UartDevice::UART3 => todo!(),
            UartDevice::UART4 => uart4_init(),
            UartDevice::UART5 => todo!(),
        }
    }

    pub unsafe fn putc(&self, c: u8) {
        let uart_base = self.device.get_base();
        while (mem_read_byte!(uart_base + 0x14) & 0x20) != 0x20 {}
        mem_write_byte!(uart_base, c);
    }

    pub unsafe fn getc(&self) -> u8 {
        let uart_base: usize = self.device.get_base();
        while (mem_read_byte!(uart_base + 0x14) & 0x1) == 0 {}
        mem_read_byte!(uart_base + 0x0)
    }

    pub unsafe fn try_getc(&self) -> Option<u8>{
        let uart_base: usize = self.device.get_base();
        if(mem_read_byte!(uart_base + 0x14) & 0x1) == 0 {
            None
        }else{
            Some(mem_read_byte!(uart_base + 0x0))
        }
    }

    pub unsafe fn puts(&self, string: &[u8]) {
        for byte in string {
            self.putc(*byte);
        }
    }
}

unsafe fn uart4_init() {
    //set uart4 transmit pin: mode0, pulldown enabled, input disabled, slew fast
    let uart4_txd = IOPin::new(PinRegister::conf_gpmc_wpn);
    uart4_txd.set(
        Some(PinMode::Mode6),
        Some(false),
        Some((PullType::Pulldown, true)),
        Some(SlewMode::Fast),
    );

    //set uart7 receive pin: mode 0, pullup enabled, input enabled, slew fast
    let uart4_rxd = IOPin::new(PinRegister::conf_mii1_txd3);
    uart4_rxd.set(
        Some(PinMode::Mode3),
        Some(true),
        Some((PullType::Pullup, true)),
        Some(SlewMode::Fast),
    );
  

    //Start a software forced wake-up on the L4 slow clock domain

    ClockCtrl::new(CM_PER_L4HS_CLKSTCTRL).unwrap().set_state(ClockCtrlState::Wakeup);

    ClockModule::new(CM_PER_UART4_CLKCTRL).unwrap().enable();

    let uart_base: usize = 0x481A8000;

    let mut temp = mem_read!(uart_base + 0x54);
    temp |= 0x2;
    mem_write!(uart_base + 0x54, temp);
    while mem_read!(uart_base + 0x58) & 1 == 0 {}
    let mut temp = mem_read_byte!(uart_base + 0x54);
    temp |= 0x1 << 3;
    mem_write_byte!(uart_base + 0x54, temp);

    while (mem_read!(uart_base + 0x14) & 0x40) != 0x40 {}

    let div: f32 = 48000000.0 / (16.0 * 19200.0);
    let intdiv: u32 = div as u32;

    mem_write_byte!(uart_base + 0x04, 0);
    mem_write_byte!(uart_base + 0x20, 0x7);
    mem_write_byte!(uart_base + 0x0C, !(0x7c));
    mem_write_byte!(uart_base + 0x00, 0);
    mem_write_byte!(uart_base + 0x04, 0);
    mem_write_byte!(uart_base + 0x0C, 0x3);
    mem_write_byte!(uart_base + 0x10, 0x3);
    mem_write_byte!(uart_base + 0x08, 0x7);
    mem_write_byte!(uart_base + 0xc, !(0x7c));
    mem_write_byte!(uart_base + 0x00, (intdiv & 0xFF) as u8);
    mem_write_byte!(uart_base + 0x04, ((intdiv >> 8) & 0x3f) as u8);
    mem_write_byte!(uart_base + 0x00, 26);
    mem_write_byte!(uart_base + 0x0c, 0x3);
    mem_write_byte!(uart_base + 0x20, 0);
}

unsafe fn uart0_init() {
    //enable option function clock on GPIO1 and Enables Module

    //wait until Module is fully active

    //set uart0 transmit pin: mode0, pulldown enabled, input disabled, slew fast
    let uart0_txd = IOPin::new(PinRegister::conf_uart0_txd);
    uart0_txd.set(
        Some(PinMode::Mode0),
        Some(false),
        Some((PullType::Pulldown, true)),
        Some(SlewMode::Fast),
    );

    //set uart0 receive pin: mode 0, pullup enabled, input enabled, slew fast
    let uart0_rxd = IOPin::new(PinRegister::conf_uart0_rxd);
    uart0_rxd.set(
        Some(PinMode::Mode0),
        Some(true),
        Some((PullType::Pullup, true)),
        Some(SlewMode::Fast),
    );

    //Start a software forced wake-up on the always on clock domain

    ClockCtrl::new(CM_WKUP_CLKSTCTRL)
        .unwrap()
        .set_state(ClockCtrlState::Wakeup);

    //Start a software forced wake-up on the L4 slow clock domain

    ClockCtrl::new(CM_PER_L4HS_CLKSTCTRL).unwrap().set_state(ClockCtrlState::Wakeup);

    ClockModule::new(CM_WKUP_UART0_CLKCTRL).unwrap().enable();

    let uart_base: usize = 0x44E09000;

    let mut temp = mem_read!(uart_base + 0x54);
    temp |= 0x2;
    mem_write!(uart_base + 0x54, temp);
    while mem_read!(uart_base + 0x58) & 1 == 0 {}
    let mut temp = mem_read_byte!(uart_base + 0x54);
    temp |= 0x1 << 3;
    mem_write_byte!(uart_base + 0x54, temp);

    while (mem_read!(uart_base + 0x14) & 0x40) != 0x40 {}

    let div: f32 = 48000000.0 / (16.0 * 115200.0);
    let intdiv: u32 = div as u32;

    mem_write_byte!(uart_base + 0x04, 0);
    mem_write_byte!(uart_base + 0x20, 0x7);
    mem_write_byte!(uart_base + 0x0C, !(0x7c));
    mem_write_byte!(uart_base + 0x00, 0);
    mem_write_byte!(uart_base + 0x04, 0);
    mem_write_byte!(uart_base + 0x0C, 0x3);
    mem_write_byte!(uart_base + 0x10, 0x3);
    mem_write_byte!(uart_base + 0x08, 0x7);
    mem_write_byte!(uart_base + 0xc, !(0x7c));
    mem_write_byte!(uart_base + 0x00, (intdiv & 0xFF) as u8);
    mem_write_byte!(uart_base + 0x04, ((intdiv >> 8) & 0x3f) as u8);
    mem_write_byte!(uart_base + 0x00, 26);
    mem_write_byte!(uart_base + 0x0c, 0x3);
    mem_write_byte!(uart_base + 0x20, 0);
}
