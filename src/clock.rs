pub mod registers;

use crate::*;

pub use registers::*;

pub enum ModuleStatus {
    Fuctional,
    Transition,
    Idle,
    Disabled,
}

pub enum ModuleMode {
    Enable,
    Disable,
}

pub enum ClockCtrlState {
    NoSleep,
    Sleep,
    Wakeup,
}

pub struct ClockCtrl {
    register: ClockRegister,
}
impl ClockCtrl {
    pub fn new(register: ClockRegister) -> Result<Self, ()> {
        if let RegisterType::Control = register.kind {
            Ok(ClockCtrl { register })
        } else {
            Err(())
        }
    }
    pub fn set_state(&self, state: ClockCtrlState) {
        unsafe {
            let mut temp = mem_read!(self.register.get_address());
            temp &= !0b11;
            mem_write!(
                self.register.get_address(),
                temp | match state {
                    ClockCtrlState::NoSleep => 0,
                    ClockCtrlState::Sleep => 1,
                    ClockCtrlState::Wakeup => 2,
                }
            );
        }
    }
}

pub struct ClockModule {
    register: ClockRegister,
}

impl ClockModule {
    pub fn new(register: ClockRegister) -> Result<Self, ()> {
        if let RegisterType::Module = register.kind {
            Ok(ClockModule { register })
        } else {
            Err(())
        }
    }
    pub fn status(&self) -> ModuleStatus {
        unsafe {
            match (mem_read!(self.register.get_address()) >> 16) & 11 {
                0 => ModuleStatus::Fuctional,
                1 => ModuleStatus::Transition,
                2 => ModuleStatus::Idle,
                3 => ModuleStatus::Disabled,
                _ => panic!("This status is not possible"),
            }
        }
    }
    pub fn set_mode(&self, mode: ModuleMode) {
        unsafe {
            let mut temp = mem_read!(self.register.get_address());
            temp &= !0b11;
            mem_write!(
                self.register.get_address(),
                temp | match mode {
                    ModuleMode::Enable => 2,
                    ModuleMode::Disable => 0,
                }
            )
        }
    }
    pub fn enable(&self) {
        self.set_mode(ModuleMode::Enable);
        while match self.status() {
            ModuleStatus::Fuctional => false,
            _ => true,
        } {}
    }
}
