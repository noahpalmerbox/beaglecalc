pub const CM_PER_BASE: usize = 0x44E00000;
pub const CM_WKUP_BASE: usize = 0x44E00400;

use RegisterType::*;

use super::ClockCtrl;
pub enum RegisterType {
    Module,
    Control,
    Unknown,
}

pub struct ClockRegister {
    pub base: usize,
    pub offset: usize,
    pub kind: RegisterType,
}

impl ClockRegister {
    pub fn get_address(&self) -> usize {
        self.base + self.offset
    }
}

//Clock Module Peripheral registers
pub const CM_PER_L4LS_CLKSTCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x0,
    kind: Control,
};
pub const CM_PER_L3S_CLKSTCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x4,
    kind: Control,
};
pub const CM_PER_L3_CLKSTCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0xC,
    kind: Control,
};
pub const CM_PER_CPGMAC0_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x14,
    kind: Unknown,
};
pub const CM_PER_LCDC_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x18,
    kind: Unknown,
};
pub const CM_PER_USB0_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x1C,
    kind: Unknown,
};
pub const CM_PER_TPTC0_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x24,
    kind: Unknown,
};
pub const CM_PER_EMIF_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x28,
    kind: Unknown,
};
pub const CM_PER_OCMCRAM_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x2C,
    kind: Unknown,
};
pub const CM_PER_GPMC_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x30,
    kind: Unknown,
};
pub const CM_PER_MCASP0_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x34,
    kind: Unknown,
};
pub const CM_PER_UART5_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x38,
    kind: Unknown,
};
pub const CM_PER_MMC0_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x3C,
    kind: Unknown,
};
pub const CM_PER_ELM_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x40,
    kind: Unknown,
};
pub const CM_PER_I2C2_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x44,
    kind: Unknown,
};
pub const CM_PER_I2C1_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x48,
    kind: Unknown,
};
pub const CM_PER_SPI0_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x4C,
    kind: Unknown,
};
pub const CM_PER_SPI1_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x50,
    kind: Unknown,
};
pub const CM_PER_L4LS_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x60,
    kind: Module,
};
pub const CM_PER_MCASP1_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x68,
    kind: Unknown,
};
pub const CM_PER_UART1_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x6C,
    kind: Unknown,
};
pub const CM_PER_UART2_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x70,
    kind: Unknown,
};
pub const CM_PER_UART3_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x74,
    kind: Unknown,
};
pub const CM_PER_UART4_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x78,
    kind: Module,
};
pub const CM_PER_TIMER7_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x7C,
    kind: Unknown,
};
pub const CM_PER_TIMER2_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x80,
    kind: Unknown,
};
pub const CM_PER_TIMER3_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x84,
    kind: Unknown,
};
pub const CM_PER_TIMER4_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x88,
    kind: Unknown,
};
pub const CM_PER_GPIO1_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0xAC,
    kind: Unknown,
};
pub const CM_PER_GPIO2_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0xB0,
    kind: Unknown,
};
pub const CM_PER_GPIO3_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0xB4,
    kind: Unknown,
};
pub const CM_PER_TPCC_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0xBC,
    kind: Unknown,
};
pub const CM_PER_DCAN0_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0xC0,
    kind: Unknown,
};
pub const CM_PER_DCAN1_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0xC4,
    kind: Unknown,
};
pub const CM_PER_EPWMSS1_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0xCC,
    kind: Unknown,
};
pub const CM_PER_EPWMSS0_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0xD4,
    kind: Unknown,
};
pub const CM_PER_EPWMSS2_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0xD8,
    kind: Unknown,
};
pub const CM_PER_L3_INSTR_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0xDC,
    kind: Module,
};
pub const CM_PER_L3_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0xE0,
    kind: Module,
};
pub const CM_PER_IEEE5000_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0xE4,
    kind: Unknown,
};
pub const CM_PER_PRU_ICSS_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0xE8,
    kind: Unknown,
};
pub const CM_PER_TIMER5_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0xEC,
    kind: Unknown,
};
pub const CM_PER_TIMER6_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0xF0,
    kind: Unknown,
};
pub const CM_PER_MMC1_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0xF4,
    kind: Unknown,
};
pub const CM_PER_MMC2_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0xF8,
    kind: Unknown,
};
pub const CM_PER_TPTC1_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0xFC,
    kind: Unknown,
};
pub const CM_PER_TPTC2_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x100,
    kind: Unknown,
};
pub const CM_PER_SPINLOCK_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x10C,
    kind: Unknown,
};
pub const CM_PER_MAILBOX0_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x110,
    kind: Unknown,
};
pub const CM_PER_L4HS_CLKSTCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x11C,
    kind: Control,
};
pub const CM_PER_L4HS_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x120,
    kind: Module,
};
pub const CM_PER_OCPWP_L3_CLKSTCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x12C,
    kind: Control,
};
pub const CM_PER_OCPWP_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x130,
    kind: Unknown,
};
pub const CM_PER_PRU_ICSS_CLKSTCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x140,
    kind: Unknown,
};
pub const CM_PER_CPSW_CLKSTCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x144,
    kind: Unknown,
};
pub const CM_PER_LCDC_CLKSTCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x148,
    kind: Unknown,
};
pub const CM_PER_CLKDIV32K_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x14C,
    kind: Unknown,
};
pub const CM_PER_CLK_24MHZ_CLKSTCTRL: ClockRegister = ClockRegister {
    base: CM_PER_BASE,
    offset: 0x150,
    kind: Unknown,
};

//Clock Module Wakeup Registers
pub const CM_WKUP_CLKSTCTRL: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x0,
    kind: Control,
};
pub const CM_WKUP_CONTROL_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x4,
    kind: Unknown,
};
pub const CM_WKUP_GPIO0_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x8,
    kind: Module,
};
pub const CM_WKUP_L4WKUP_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0xC,
    kind: Unknown,
};
pub const CM_WKUP_TIMER0_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x10,
    kind: Module,
};
pub const CM_WKUP_DEBUGSS_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x14,
    kind: Unknown,
};
pub const CM_L3_AON_CLKSTCTRL: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x18,
    kind: Unknown,
};
pub const CM_AUTOIDLE_DPLL_MPU: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x1C,
    kind: Unknown,
};
pub const CM_IDLEST_DPLL_MPU: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x20,
    kind: Unknown,
};
pub const CM_SSC_DELTAMSTEP_DPLL_MPU: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x24,
    kind: Unknown,
};
pub const CM_SSC_MODFREQDIV_DPLL_MPU: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x28,
    kind: Unknown,
};
pub const CM_CLKSEL_DPLL_MPU: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x2C,
    kind: Unknown,
};
pub const CM_AUTOIDLE_DPLL_DDR: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x30,
    kind: Unknown,
};
pub const CM_IDLEST_DPLL_DDR: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x34,
    kind: Unknown,
};
pub const CM_SSC_DELTAMSTEP_DPLL_DDR: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x38,
    kind: Unknown,
};
pub const CM_SSC_MODFREQDIV_DPLL_DDR: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x3C,
    kind: Unknown,
};
pub const CM_CLKSEL_DPLL_DDR: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x40,
    kind: Unknown,
};
pub const CM_AUTOIDLE_DPLL_DISP: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x44,
    kind: Unknown,
};
pub const CM_IDLEST_DPLL_DISP: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x48,
    kind: Unknown,
};
pub const CM_SSC_DELTAMSTEP_DPLL_DISP: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x4C,
    kind: Unknown,
};
pub const CM_SSC_MODFREQDIV_DPLL_DISP: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x50,
    kind: Unknown,
};
pub const CM_CLKSEL_DPLL_DISP: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x54,
    kind: Unknown,
};
pub const CM_AUTOIDLE_DPLL_CORE: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x58,
    kind: Unknown,
};
pub const CM_IDLEST_DPLL_CORE: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x5C,
    kind: Unknown,
};
pub const CM_SSC_DELTAMSTEP_DPLL_CORE: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x60,
    kind: Unknown,
};
pub const CM_SSC_MODFREQDIV_DPLL_CORE: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x64,
    kind: Unknown,
};
pub const CM_CLKSEL_DPLL_CORE: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x68,
    kind: Unknown,
};
pub const CM_AUTOIDLE_DPLL_PER: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x6C,
    kind: Unknown,
};
pub const CM_IDLEST_DPLL_PER: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x70,
    kind: Unknown,
};
pub const CM_SSC_DELTAMSTEP_DPLL_PER: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x74,
    kind: Unknown,
};
pub const CM_SSC_MODFREQDIV_DPLL_PER: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x78,
    kind: Unknown,
};
pub const CM_CLKDCOLDO_DPLL_PER: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x7C,
    kind: Unknown,
};
pub const CM_DIV_M4_DPLL_CORE: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x80,
    kind: Unknown,
};
pub const CM_DIV_M5_DPLL_CORE: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x84,
    kind: Unknown,
};
pub const CM_CLKMODE_DPLL_MPU: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x88,
    kind: Unknown,
};
pub const CM_CLKMODE_DPLL_PER: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x8C,
    kind: Unknown,
};
pub const CM_CLKMODE_DPLL_CORE: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x90,
    kind: Unknown,
};
pub const CM_CLKMODE_DPLL_DDR: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x94,
    kind: Unknown,
};
pub const CM_CLKMODE_DPLL_DISP: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x98,
    kind: Unknown,
};
pub const CM_CLKSEL_DPLL_PERIPH: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0x9C,
    kind: Unknown,
};
pub const CM_DIV_M2_DPLL_DDR: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0xA0,
    kind: Unknown,
};
pub const CM_DIV_M2_DPLL_DISP: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0xA4,
    kind: Unknown,
};
pub const CM_DIV_M2_DPLL_MPU: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0xA8,
    kind: Unknown,
};
pub const CM_DIV_M2_DPLL_PER: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0xAC,
    kind: Unknown,
};
pub const CM_WKUP_WKUP_M3_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0xB0,
    kind: Unknown,
};
pub const CM_WKUP_UART0_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0xB4,
    kind: Module,
};
pub const CM_WKUP_I2C0_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0xB8,
    kind: Unknown,
};
pub const CM_WKUP_ADC_TSC_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0xBC,
    kind: Unknown,
};
pub const CM_WKUP_SMARTREFLEX0_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0xC0,
    kind: Unknown,
};
pub const CM_WKUP_TIMER1_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0xC4,
    kind: Unknown,
};
pub const CM_WKUP_SMARTREFLEX1_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0xC8,
    kind: Unknown,
};
pub const CM_L4_WKUP_AON_CLKSTCTRL: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0xCC,
    kind: Unknown,
};
pub const CM_WKUP_WDT1_CLKCTRL: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0xD4,
    kind: Unknown,
};
pub const CM_DIV_M6_DPLL_CORE: ClockRegister = ClockRegister {
    base: CM_WKUP_BASE,
    offset: 0xD8,
    kind: Unknown,
};
